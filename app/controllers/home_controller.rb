class HomeController < ApplicationController
  def index
    @page_description = "A highly skilled software engineer working with Ruby on Rails, Java and C#, writing about software development and 3D graphics."
    render layout: 'homepage', text: ""
  end
end
