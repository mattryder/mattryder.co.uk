class ChangeArticleDataType < ActiveRecord::Migration
  def change
    change_column :posts, :article, :text
  end
end
