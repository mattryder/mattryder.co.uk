Rails.application.routes.draw do

  mount RedactorRails::Engine => '/redactor_rails'
  devise_for :admins, skip: :registrations
  resources :posts

  get 'home/index'
  get '*path' => redirect("/")

  root "home#index"
end
